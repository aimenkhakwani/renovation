$(document).ready(function() {
  //switching before and after pics
  $(".sample1").click (function(){
    $("#shack1").toggle();
    $("#mansion1").toggle();
  });
  $(".sample2").click (function(){
    $("#shack2").toggle();
    $("#mansion2").toggle();
  });


  //switching between color schemes
  $("#darkScheme").click(function(){
    $("body").removeClass();
    $("body").addClass("dark");
  });

  $("#lightScheme").click(function(){
    $("body").removeClass();
    $("body").addClass("light");
  });

  $("#originalScheme").click(function(){
    $("body").removeClass();
    $("body").addClass("original");
  });

});
